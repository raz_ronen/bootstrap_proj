const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');

// Compile Sass & Inject Into Browser
gulp.task('sass', function(){
    // sass file we want to compile
    // bootstrap.scss - import all scss components inside it, so we compile all components.
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss',
        'src/scss/*.scss'])
        .pipe(sass())                // that's the plugin that compile
        .pipe(gulp.dest("src/css"))  // tell it where to compile to (create folder for us)
        .pipe(browserSync.stream()); // activating the browser sync
});

// Move JS Files to src/js
gulp.task('js', function(){
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js'])
        .pipe(gulp.dest('src/js'))
        .pipe(browserSync.stream());
});

// Watch Sass & Server
gulp.task('serve',['sass'], function(){
    browserSync.init({
       server: './src' //load it to the server
    });

    // continue watch our sass so we compile it every time we save it.
    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss',
        'src/scss/*.scss'], ['sass']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

// Move Fonts Folder to src/fonts
gulp.task('fonts', function(){
   return gulp.src('node_modules/font-awesome/fonts/*')
       .pipe(gulp.dest("src/fonts"));
});

// Move Fonts Awesome CSS to src/css
gulp.task('fa', function(){
    return gulp.src('node_modules/font-awesome/css/font-awesome.min.css')
        .pipe(gulp.dest("src/css"));
});

//When we run 'gulp' we run all the things we define
gulp.task('default', ['js','serve','fa','fonts']);

//to be able run gulp we had to do:
// npm install -g gulp-cli